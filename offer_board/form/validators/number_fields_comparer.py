from wtforms import ValidationError


class NumberFieldsComparer:
    def __init__(self, message=None):
        if not message:
            message = 'Money max must be greater than money min'
        self.message = message

    def __call__(self, form, field):
        money_max = form.money_max.data
        if money_max is not None and field.data > money_max:
            raise ValidationError(self.message)