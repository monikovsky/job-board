from offer_board import db, app

from offer_board.model.account import Account
from offer_board.model.company import Company
from offer_board.model.offer_category import OfferCategory
from offer_board.model.offer import Offer
from offer_board.model.offer_currency_type import OfferCurrencyType
from offer_board.model.offer_payment_type import OfferPaymentType

db.drop_all()
db.create_all()
