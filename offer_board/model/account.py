from offer_board import db
from flask_login import UserMixin
from datetime import datetime


class Account(db.Model, UserMixin):
    # auth info
    id = db.Column(db.Integer, primary_key=True)
    username = db.Column(db.String(20), unique=True, nullable=False)
    password = db.Column(db.String(50), unique=False, nullable=False)

    # info about user
    first_name = db.Column(db.String(50), unique=False, nullable=False)
    last_name = db.Column(db.String(50), unique=False, nullable=False)
    email = db.Column(db.String(100), unique=True, nullable=False)
    img_path = db.Column(db.String(300), unique=False, nullable=True, default='public/image/upload/img.png')
    contact_email = db.Column(db.String(100), unique=True, nullable=True)
    phone = db.Column(db.Numeric(9), unique=True, nullable=True)
    locations = db.Column(db.String(250), unique=False, nullable=True)
    description = db.Column(db.Text(2500), unique=False, nullable=True)
    remote = db.Column(db.Boolean, unique=False, nullable=False, default=False)
    is_company = db.Column(db.Boolean, unique=False, nullable=False)
    website = db.Column(db.String(250), unique=False, nullable=True)
    facebook = db.Column(db.String(250), unique=False, nullable=True)
    github = db.Column(db.String(250), unique=False, nullable=True)
    twitter = db.Column(db.String(250), unique=False, nullable=True)
    linked_in = db.Column(db.String(250), unique=False, nullable=True)
    slug = db.Column(db.String(100), unique=True, nullable=False)

    created_at = db.Column(db.DateTime, unique=False, nullable=False, default=datetime.now())
    updated_at = db.Column(db.DateTime, unique=False, nullable=False, default=datetime.now())
    active = db.Column(db.Boolean, unique=False, nullable=False, default=True)  # true for dev

    # relationships
    offers = db.relationship('Offer', backref='account', lazy=True)
    company = db.relationship('Company', backref='account', lazy=True, uselist=False)
    specialist = db.relationship('Specialist', backref='account', lazy=True, uselist=False)


