from offer_board import db


class Company(db.Model):
    id = db.Column(db.Integer, primary_key=True)
    company_name = db.Column(db.String(100), unique=False, nullable=True)
    company_industry = db.Column(db.String(100), unique=False, nullable=True)
    company_size = db.Column(db.String(10), unique=False, nullable=True)
    account_id = db.Column(db.Integer, db.ForeignKey('account.id'))