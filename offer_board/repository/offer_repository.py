from ..model.offer import Offer


class OfferRepository:
    def __init__(self, query):
        self.query = query

    def filter_offers_by_all_given_parameters(
            self,
            offer_id=None,
            title=None,
            description=None,
            slug=None,
            employer_name=None,
            location=None,
            money=None,
            created_at=None,
            currencies=None,
            payments=None,
            types=None,
            categories=None,
    ) -> list:

        if title:
            self.query = self.query.filter(Offer.title.like("%" + title + "%"))

        if location:
            self.query = self.query.filter(Offer.location.like("%" + location + "%"))

        if employer_name:
            self.query = self.query.filter(Offer.employer_name.like("%" + employer_name + "%"))

        if categories:
            self.query = self.query.filter(Offer.offer_category_id.in_(categories))

        if payments:
            self.query = self.query.filter(Offer.offer_payment_type_id.in_(payments))

        if types:
            self.query = self.query.filter(Offer.offer_type_id.in_(types))

        if currencies:
            self.query = self.query.filter(Offer.offer_currency_type_id.in_(currencies))

        if money:
            self.query = self.query.filter(Offer.money_max >= money)

        # todo filter by date: example: 1hour ago, 1day ago, 7days ago, 30days ago
        if created_at:
            pass

        return self.query.all()
