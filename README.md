# job board project based on flask - co-authored with @Vrocode

## implemented features - creating an account, logging in and out, adding offers, listing offers, searching for offers with filtering, functional site

LINUX

to run this project use:

1. `python3 -m venv venv` - create virtual environment
2. `source venv/bin/activate` - activate the created virtual environment
3. `pip3 install -r requirements.txt` - install dependencies
4. `python3 create_db.py` - create db
5. run: `gunicorn offer_board:app`
6. go to 127.0.0.1:8000/migration to create and link categories
7. 127.0.0.1:8000 for the website