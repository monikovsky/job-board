import os

from flask import render_template, redirect, url_for, abort, Blueprint, request, flash
from flask_login import login_required, current_user
from werkzeug.utils import secure_filename

from .. import db, app
from ..form.offer_form import OfferForm
from ..model.offer import Offer
from ..model.offer_category import OfferCategory
from ..model.offer_currency_type import OfferCurrencyType
from ..model.offer_payment_type import OfferPaymentType
from ..model.offer_type import OfferType
from ..service.general_utils import create_slug
from flask_sqlalchemy import Model
from ..model.offer_payment_type import OfferPaymentType
from ..model.offer_currency_type import OfferCurrencyType
from ..form.offer_filter_form import OfferFilterForm
from sqlalchemy import or_
from ..repository.offer_repository import OfferRepository
from ..service.general_utils import get_unique_filename, uploaded_image_file_exists

offer = Blueprint(
    name='offer',
    import_name=__name__,
    template_folder='../templates/offer',
    static_folder='../public'
)


@offer.route('/', methods=['GET', 'POST'])
def index():
    return render_template(
        'index.html',
        offers=Offer.query.order_by(Offer.id.desc()).limit(5)
    )



@offer.route('/offers', methods=['GET', 'POST'])
def offers_show():
    filters_form = OfferFilterForm()

    if filters_form.validate_on_submit():
        offer_repository = OfferRepository(Offer.query)

        offers = offer_repository.filter_offers_by_all_given_parameters(
            title=filters_form.title.data,
            employer_name=filters_form.employer_name.data,
            location=filters_form.location.data,
            money=filters_form.money.data,
            currencies=[currency.id for currency in filters_form.currencies.data],
            payments=[payment.id for payment in filters_form.payments.data],
            types=[type.id for type in filters_form.types.data],
            categories=[category.id for category in filters_form.categories.data],
        )
    else:
        offers = Offer.query.all()

    return render_template(
        'offers_show.html',
        offers=offers,
        filters_form=filters_form
    )


@offer.route('/offers/<string:slug>')
def offer_show(slug):
    offer = Offer.query.filter_by(slug=slug).first()
    if offer is None:
        abort(404)

    return render_template(
        'offer_show.html',
        offer=offer
    )


@offer.route('/offers/<string:slug>/edit')
@login_required
def offer_edit(slug):
    offer = Offer.query.filter_by(slug=slug, account_id=current_user.id).first()
    if offer is None:
        abort(403)

    return render_template(
        'offer_edit.html',
        offer=offer
    )


@offer.route('/new-offer', methods=['GET', 'POST'])
@login_required
def offer_add():
    form = OfferForm()
    if form.validate_on_submit():
        offer = Offer()
        offer.slug = create_slug(form.title.data)

        img_field = form.image.name
        if img_field in request.files and request.files[img_field].filename:
            image = request.files[img_field]
            filename = get_unique_filename(image.filename)
            image.save(os.path.join(app.root_path + "/" + app.config['UPLOAD_FOLDER'], filename))
            offer.img_path = os.path.join(app.config['UPLOAD_FOLDER'], filename)

        offer.account_id = current_user.id
        offer.offer_category_id = form.categories.data.id
        offer.offer_type_id = form.types.data.id
        offer.offer_currency_type_id = form.currencies.data.id
        offer.offer_payment_type_id = form.payments.data.id

        form.populate_obj(offer)  # Copies matching attributes from form onto user
        db.session.add(offer)
        db.session.commit()
        flash('Oferta została dodana!')
        return redirect(url_for('offer.index'))

    return render_template(
        'offer_new.html',
        form=form
    )


@offer.route('/migration')
def temp_migration():
    offer_cat = OfferCategory.query.all()
    if len(offer_cat) > 0:
        db.session.query(OfferCategory).delete()
        db.session.commit()

    categoriesList = [
        'Graphics',
        'Software development',
        'UX/UI',
        'Customer Support',
        'Call Center',
    ]
    for i in categoriesList:
        cat = OfferCategory(name=i)
        db.session.add(cat)

    opt = OfferPaymentType()

    octList = ['EUR', 'PLN', 'USD']

    for l in octList:
        oct = OfferCurrencyType()
        oct.currency = l
        oct.multiplier = 4
        db.session.add(oct)

    optList = ['Platnosc miesieczna', 'Platnosc godzinowa', 'Pieniadze za zlecenie']
    optSuffixList = ['miesięcznie brutto', 'h brutto', 'brutto']
    for i in range(3):
        opt = OfferPaymentType()
        opt.name = optList[i]
        opt.suffix = optSuffixList[i]
        db.session.add(opt)

    offer_types = OfferType.query.all()
    if len(offer_types) > 0:
        db.session.query(OfferType).delete()
        db.session.commit()

    typesList = [
        'Zlecenie',
        'Ogłoszenie o prace',
    ]
    for i in typesList:
        type = OfferType(name=i)
        db.session.add(type)

    db.session.commit()
    return redirect('/')
