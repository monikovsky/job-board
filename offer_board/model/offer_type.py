from offer_board import db

"""

table offers {
  id int [pk, increment]
  user_id int [ref: > user.id]
  title varchar
  category enum 
  salary varchar
}

"""


class OfferType(db.Model):
    id = db.Column(db.Integer, primary_key=True)
    name = db.Column(db.String(50), unique=True, nullable=False)
    offers = db.relationship('Offer', backref='offer_type', lazy=True)

