from ..model.specialist import Specialist


class SpecialistRepository:
    def __init__(self, query):
        self.query = query

    def filter_specialists_by_all_given_parameters(
            self,
            skills=None,
    ) -> list:

        if skills:
            for s in skills.split():
                self.query = self.query.filter(Specialist.skills.like("%" + s + "%"))

        return self.query.all()
