from flask_wtf import FlaskForm
from wtforms import StringField, TextAreaField, SubmitField, BooleanField, FileField, IntegerField, \
    ValidationError
from wtforms.ext.sqlalchemy.fields import QuerySelectField
from wtforms.validators import DataRequired, Length, Optional, NumberRange
from wtforms.widgets import html5
from ..model.offer_category import OfferCategory
from ..model.offer_currency_type import OfferCurrencyType
from ..model.offer_payment_type import OfferPaymentType
from ..model.offer_type import OfferType
from .validators.mime_type import MimeType
from .validators.file_size import FileSize
from .validators.number_fields_comparer import NumberFieldsComparer


class OfferForm(FlaskForm):
    title = StringField('title', validators=[DataRequired(), Length(max=100, min=3)])

    description = TextAreaField('description', validators=[DataRequired(), Length(max=100, min=3)])

    money = IntegerField('money',
                         validators=[DataRequired(), NumberRange(min=0, max=999999999), NumberFieldsComparer()],
                         widget=html5.NumberInput())

    money_max = IntegerField('money_max', validators=[Optional(), NumberRange(min=0, max=999999999)],
                             widget=html5.NumberInput())

    categories = QuerySelectField(query_factory=lambda: OfferCategory.query, get_label='name')

    types = QuerySelectField(query_factory=lambda: OfferType.query, get_label='name')

    currencies = QuerySelectField(query_factory=lambda: OfferCurrencyType.query, get_label='currency')

    payments = QuerySelectField(query_factory=lambda: OfferPaymentType.query, get_label='name')

    remote = BooleanField('remote', validators=[])

    employer_name = StringField('employer_name', validators=[DataRequired(), Length(max=100, min=3)])

    location = StringField('location', validators=[DataRequired(), Length(max=100, min=3)])

    image = FileField('image', validators=[
        Optional(),
        FileSize(max_file_size_in_bytes=0.2 * 1024 * 1024),
        MimeType(['image/jpeg', 'image/png'])])

    submit = SubmitField('submit')
