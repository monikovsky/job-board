from flask_wtf import FlaskForm
from wtforms import StringField, SubmitField, PasswordField, IntegerField, BooleanField, SelectMultipleField, \
    DateTimeField
from wtforms.validators import NumberRange, DataRequired, Optional
from wtforms.widgets import html5, CheckboxInput, ListWidget
from wtforms.ext.sqlalchemy.fields import QuerySelectField, QuerySelectMultipleField
from ..model.offer_category import OfferCategory
from ..model.offer_payment_type import OfferPaymentType
from ..model.offer_currency_type import OfferCurrencyType
from ..model.offer_type import OfferType


class SpecialistFilterForm(FlaskForm):
    skills = StringField('Skills', validators=[Optional()])
    # todo fix that

    submit = SubmitField('submit')
