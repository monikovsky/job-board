from offer_board import db


class OfferPaymentType(db.Model):
    id = db.Column(db.Integer, primary_key=True)
    name = db.Column(db.String(50), unique=True, nullable=False)
    suffix = db.Column(db.String(50), unique=True, nullable=False)
    offers = db.relationship('Offer', backref='offer_payment_type', lazy=True)
