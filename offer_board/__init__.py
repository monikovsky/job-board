from flask import Flask, redirect, render_template
from flask_sqlalchemy import SQLAlchemy
from .config import Config
from flask_login import LoginManager
from flask_authorize import Authorize

config = Config()

app = Flask(__name__)

app.config['SQLALCHEMY_DATABASE_URI'] = config.DATABASE_URI
app.config['SQLALCHEMY_TRACK_MODIFICATIONS'] = False
app.config['SECRET_KEY'] = config.SECRET_KEY
app.config['UPLOAD_FOLDER'] = config.UPLOAD_FOLDER

db = SQLAlchemy(app)
login_manager = LoginManager()
login_manager.init_app(app)
authorize = Authorize(app)


from .controller.offer_controller import offer
app.register_blueprint(offer)
from .controller.account_controller import account
app.register_blueprint(account)
from .controller.specialist_controller import specialist
app.register_blueprint(specialist)
