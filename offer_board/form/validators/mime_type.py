from wtforms import ValidationError
import magic
import mimetypes
from collections.abc import Iterable
from werkzeug.datastructures import FileStorage


class MimeType(object):
    def __init__(self, mime_types, message=None):
        if not isinstance(mime_types, Iterable):
            raise TypeError('mime_types must by iterable.')
        self.mime_types = mime_types
        if not message:
            message = 'Image must be type : {extensions}'.format(extensions=', '.join(mime_types))
        self.message = message

    def __call__(self, form, field):
        if not (isinstance(field.data, FileStorage) and field.data):
            return
        mime_type = magic.from_buffer(field.data.read(), mime=True)
        field.data.seek(0)
        if mime_type not in self.mime_types:
            raise ValidationError(self.message)
