from offer_board import db
from flask_login import UserMixin


class Specialist(db.Model):
    id = db.Column(db.Integer, primary_key=True)
    relocate = db.Column(db.Boolean, unique=False, nullable=True)
    skills = db.Column(db.String(250), unique=False, nullable=True)
    account_id = db.Column(db.Integer, db.ForeignKey('account.id'))