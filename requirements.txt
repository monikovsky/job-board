python-magic==0.4.18
Click==7.0
Flask==1.1.1
Flask-Authorize==0.1.8
Flask-Login==0.4.1
Flask-SQLAlchemy==2.4.1
Flask-Uploads==0.2.1
Flask-WTF==0.14.2
gunicorn==20.0.4
itsdangerous==1.1.0
Jinja2==2.10.3
libmagic==1.0
MarkupSafe==1.1.1
names==0.3.0
six==1.13.0
SQLAlchemy==1.3.12
virtualenv==16.7.9
Werkzeug==0.16.0
WTForms==2.2.1
