from offer_board import db


class OfferCurrencyType(db.Model):
    id = db.Column(db.Integer, primary_key=True)
    currency = db.Column(db.String(10), nullable=False, unique=True)
    multiplier = db.Column(db.Integer, nullable=False, unique=False)

    offers = db.relationship('Offer', backref='offer_currency_type', lazy=True)

