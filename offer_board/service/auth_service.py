from flask_login import current_user
from flask import abort, redirect, flash
from functools import wraps


def company_required(func):
    @wraps(func)
    def decorated_function(*args, **kwargs):
        if not current_user.is_company:
            abort(404)
        return func(*args, **kwargs)
    return decorated_function