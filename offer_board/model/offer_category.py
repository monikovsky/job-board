from offer_board import db


class OfferCategory(db.Model):
    id = db.Column(db.Integer, primary_key=True)
    name = db.Column(db.String(100), unique=False, nullable=False)
    offers = db.relationship('Offer', backref='offer_category', lazy=True)
