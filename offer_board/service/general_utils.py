import uuid
from flask import request
from werkzeug import secure_filename
import os

"""
custom slug'inator
It just make url-safe slug variable
@:return str
@:param *args
"""


def create_slug(*args) -> str:
    slug = ''
    for arg in args:
        # replace all spaces with hyphens
        arg = str(arg).replace(' ', '-')
        # filter any character which can be dangerous, let hyphen leave it own life
        safeArg = list([val for val in arg if val.isalnum() or val == '-'])
        # join the safeArg list
        slug += "".join(safeArg) + '-'
    # add uuid to slug - safe reasons, without may be duplicate
    slug += str(uuid.uuid1())
    return slug


def get_unique_filename(filename):
    basename, file_extension = os.path.splitext(filename)
    return secure_filename(basename) + create_slug(basename) + secure_filename(file_extension)


def uploaded_image_file_exists(field_name="image"):
    if field_name in request.files and request.files[field_name].filename:
        return True
    return False
