from flask_wtf import FlaskForm
from wtforms import StringField, SubmitField, PasswordField, IntegerField, BooleanField, SelectMultipleField, DateTimeField
from wtforms.validators import NumberRange, DataRequired, Optional
from wtforms.widgets import html5, CheckboxInput, ListWidget
from wtforms.ext.sqlalchemy.fields import QuerySelectField, QuerySelectMultipleField
from ..model.offer_category import OfferCategory
from ..model.offer_payment_type import OfferPaymentType
from ..model.offer_currency_type import OfferCurrencyType
from ..model.offer_type import OfferType


class MultiCheckboxField(QuerySelectMultipleField):
    widget = ListWidget(prefix_label=False)
    option_widget = CheckboxInput()


class OfferFilterForm(FlaskForm):
    title = StringField('Tytul', validators=[Optional()])

    location = StringField('Lokalizacja', validators=[Optional()])

    employer_name = StringField('nazwa pracodawcy', validators=[Optional()])

    money = IntegerField('Zarobki minimalne (brutto)', validators=[NumberRange(min=0, max=999999999), Optional()], widget=html5.NumberInput())

    categories = MultiCheckboxField(query_factory=lambda: OfferCategory.query, validators=[Optional()], get_label='name')

    types = MultiCheckboxField(query_factory=lambda: OfferType.query, validators=[Optional()], get_label='name')

    currencies = MultiCheckboxField(query_factory=lambda: OfferCurrencyType.query, validators=[Optional()], get_label='currency')

    payments = MultiCheckboxField(query_factory=lambda: OfferPaymentType.query, validators=[Optional()], get_label='name')


    # todo fix that
    created_at = DateTimeField('created_at')

    submit = SubmitField('submit')
