class Config:
    DATABASE_URI = 'sqlite:///database.db'
    SECRET_KEY = 'dev'
    UPLOAD_FOLDER = 'public/image/upload/'
