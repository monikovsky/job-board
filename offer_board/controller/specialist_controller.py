from flask import render_template, redirect, flash, Blueprint, url_for, request
from ..model.specialist import Specialist
from ..model.account import Account
from ..model.company import Company
from flask_login import login_user, login_required, logout_user
from ..form.login_form import LoginForm
from ..form.register_form import RegisterForm
from .. import login_manager, db, app
from ..service.general_utils import create_slug
import os
from werkzeug.utils import secure_filename
from ..repository.specialist_repository import SpecialistRepository
from ..form.user_filter_form import SpecialistFilterForm


specialist = Blueprint(
    name='specialist',
    import_name=__name__,
    template_folder='../templates/specialist',
    static_folder='../public'
)


@specialist.route('/specialist/{string:slug}')
def user_show(slug):
    return render_template(
        'specialist_show.html',
        specialist=Specialist.query.filter_by(slug=slug).first(),
    )


@specialist.route('/specialist/{string:slug}/edit')
def user_edit(slug):
    return render_template(
        'specialist_show.html',
        user=Specialist.query.filter_by(slug=slug).first(),
    )


# todo get a better name for this endpoint
@specialist.route('/specialists', methods=['GET', 'POST'])
def specialists_show():
    filters_form = SpecialistFilterForm()

    if filters_form.validate_on_submit():
        specialist_repository = SpecialistRepository(Specialist.query)

        specialists = specialist_repository.filter_specialists_by_all_given_parameters(
            skills=filters_form.skills.data
        )
    else:
        specialists = Specialist.query.all()

    return render_template(
        'specialists_show.html',
        specialists=specialists,
        filters_form=filters_form
    )