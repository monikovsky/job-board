import os

from flask import render_template, redirect, flash, Blueprint, url_for, request, abort
from flask_login import login_user, login_required, logout_user, current_user

from .. import login_manager, db, app
from ..form.account_edit_form import AccountEditForm
from ..form.login_form import LoginForm
from ..form.register_form import RegisterForm
from ..model.account import Account
from ..model.company import Company
from ..model.specialist import Specialist
from ..service.general_utils import create_slug, get_unique_filename

account = Blueprint(
    name='account',
    import_name=__name__,
    template_folder='../templates/account',
    static_folder='../public'
)


@account.route('/login', methods=['GET', 'POST'])
def login():
    form = LoginForm()

    if form.validate_on_submit():
        # Login and validate the user.
        # user should be an instance of your `User` class
        account = Account.query.filter_by(username=form.username.data).first()
        if (account is None) or (account.password != form.password.data):
            flash('Username or password is incorrect.')
            return redirect('/')

        login_user(account)
        flash('Logged in successfully.')
        return redirect('/')

    return render_template('login.html', form=form)


@login_manager.user_loader
def load_user(account_id):
    return Account.query.get(account_id)


@account.route('/register', methods=['POST', 'GET'])
def register():
    form = RegisterForm()

    if form.validate_on_submit():
        if Account.query.filter_by(email=form.email.data, username=form.username.data).first() is not None:
            flash('User or email already exists.')
            return redirect(url_for('account.register'))

        account = Account()
        img_field = form.image.name
        if img_field in request.files and request.files[img_field].filename:
            image = request.files[img_field]
            filename = get_unique_filename(image.filename)
            image.save(os.path.join(app.root_path + "/" + app.config['UPLOAD_FOLDER'], filename))
            account.img_path = os.path.join(app.config['UPLOAD_FOLDER'], filename)

        form.populate_obj(account)  # Copies matching attributes from form onto user
        account.slug = create_slug(account.first_name, account.last_name)
        db.session.add(account)
        db.session.commit()

        account_type = Company() if account.is_company else Specialist()

        account_type.account_id = account.id

        db.session.add(account_type)
        db.session.commit()
        flash('Konto zostało utworzone!')
        return redirect(url_for('offer.index'))

    return render_template(
        'register.html',
        form=form
    )


@account.route('/logout')
@login_required
def logout():
    logout_user()
    return redirect(url_for('offer.index'))


@account.route('/account/edit')
@login_required
def account_edit(id):
    account = Account.query.filter_by(id=current_user.id).first()

    if not account:
        abort(404)

    form = AccountEditForm()
    return render_template(
        'account_edit.html',
        form=form
    )
