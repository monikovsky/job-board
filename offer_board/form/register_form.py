import magic
import mimetypes
from flask_wtf import FlaskForm
from flask_wtf.file import FileAllowed
from wtforms import StringField, SubmitField, PasswordField, BooleanField, FileField, ValidationError
from wtforms.validators import DataRequired, Optional
from .validators.file_size import FileSize
from .validators.mime_type import MimeType


class RegisterForm(FlaskForm):
    username = StringField('username', validators=[DataRequired()])
    password = PasswordField('password', validators=[DataRequired()])
    first_name = StringField('username', validators=[DataRequired()])
    last_name = StringField('username', validators=[DataRequired()])
    email = StringField('username', validators=[DataRequired()])
    is_company = BooleanField('is_company', validators=[])
    image = FileField('image', validators=[Optional(), Optional(),
                                           FileSize(max_file_size_in_bytes=0.2 * 1024 * 1024),
                                           MimeType(['image/jpeg', 'image/png'])])
    submit = SubmitField('submit')
