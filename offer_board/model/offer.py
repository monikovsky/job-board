from offer_board import db
from datetime import datetime

"""

table offers {
  id int [pk, increment]
  user_id int [ref: > user.id]
  title varchar
  category enum 
  salary varchar
}

"""


class Offer(db.Model):
    id = db.Column(db.Integer, primary_key=True)
    title = db.Column(db.String(100), unique=False, nullable=False)
    description = db.Column(db.TEXT(5000))
    slug = db.Column(db.String(100), unique=True, nullable=False)
    employer_name = db.Column(db.String(100), unique=False, nullable=False)
    location = db.Column(db.String(100), unique=False, nullable=False)
    money = db.Column(db.Integer(), unique=False, nullable=True)
    money_max = db.Column(db.Integer(), unique=False, nullable=True)
    created_at = db.Column(db.DateTime, unique=False, nullable=False, default=datetime.now())
    updated_at = db.Column(db.DateTime, unique=False, nullable=False, default=datetime.now(), onupdate=datetime.now())
    active = db.Column(db.Boolean, unique=False, nullable=False, default=True)  # true for dev

    # relations
    offer_category_id = db.Column(db.Integer, db.ForeignKey('offer_category.id'), nullable=False)
    account_id = db.Column(db.Integer, db.ForeignKey('account.id'), nullable=False)
    offer_type_id = db.Column(db.Integer, db.ForeignKey('offer_type.id'), nullable=False)
    offer_currency_type_id = db.Column(db.Integer, db.ForeignKey('offer_currency_type.id'), nullable=True)
    offer_payment_type_id = db.Column(db.Integer, db.ForeignKey('offer_payment_type.id'), nullable=True)
    img_path = db.Column(db.String(300), unique=False, nullable=True, default='public/image/upload/img.png')
