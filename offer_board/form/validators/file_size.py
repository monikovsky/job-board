from wtforms import ValidationError
from werkzeug.datastructures import FileStorage


class FileSize(object):

    def __init__(self, max_file_size_in_bytes, message=None):
        self.max_file_size_in_bytes = max_file_size_in_bytes
        if not message:
            message = 'File must be smaller than %s megabytes' % str(max_file_size_in_bytes/1024/1024)
        self.message = message

    def __call__(self, form, field):
        if not (isinstance(field.data, FileStorage) and field.data):
            return
        file_size = len(field.data.read())
        field.data.seek(0)
        if file_size > self.max_file_size_in_bytes:
            raise ValidationError(self.message)
